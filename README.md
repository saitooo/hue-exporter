# Hue exporter

This is an `inputs.exec` script for Telegraf running on my Raspberry Pi Zero W.
It queries the Philips Hue bridge (in my case it's the squared version) for the existing lights or groups returning a JSON object, plain and simple.

I have defined two `inputs.exec` sections in my `telegraf.conf`:
```
[[inputs.exec]]
  command = "/usr/local/bin/hue HueLights"
  interval = "1m"
  timeout = "10s"
  data_format = "json"
  name_override = "hue_light_metrics"
  tag_keys = [
    "hue_light_name",
    "hue_light_model",
  ]
  json_string_fields = [
    "hue_light_reachable",
    "hue_light_on",
  ]
```
```
[[inputs.exec]]
  command = "/usr/local/bin/hue HueGroups"
  interval = "1m"
  timeout = "10s"
  data_format = "json"
  name_override = "hue_group_metrics"
  tag_keys = [
    "hue_group_name",
    "hue_group_class"
  ]
  json_string_fields = [
    "hue_group_all_on",
    "hue_group_any_on"
  ]
```